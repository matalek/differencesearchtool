/**
 * Wyznacza korelację kolorów na podstawie odległości euklidesowej kolorów 
 * w formacie RGB.
 */

package differencesearchtool;

import java.awt.Color;
import static java.lang.Math.sqrt;

public class RGBCorrelationCalculator implements CorrelationCalculator {

    @Override
    public int calculateCorrelation(Color col1, Color col2) {
        
        // kwadrat odległości euklidesowej
        double d = Math.pow((col1.getRed() - col2.getRed()), 2)
                + Math.pow((col1.getGreen() - col2.getGreen()), 2)
                + Math.pow((col1.getBlue() - col2.getBlue()), 2);
        
        double correlation = (1. - sqrt(d / (double) MAX_DISTANCE_SQUARE)) * 255.;
        return (int) correlation;
    }
    
    private static int MAX_DISTANCE_SQUARE = 255 * 255 * 3;

}
