/**
 * Interfejs pozwalający wyznaczać korelację dwóch kolorów zgodnie z przyjętą
 * metodą.
 */

package differencesearchtool;

import java.awt.Color;

public interface CorrelationCalculator {
    
    // przyjmuje dwa kolory i zwraca wyliczoną na podstawie odpowiedniej metody 
    // korelację między nimi jako liczbę całkowitą z przedziału 0 - 255, 
    // gdzie 0 oznacza najmniejszą korelację, a 255 największą
    public int calculateCorrelation(Color col1, Color col2);
    
}
