/**
 * Wyznacza korelację kolorów na podstawie konwersji do L*ab oraz
 * formuły CIE76.
 * 
 * Algorytm zamiany RGB na L*ab zaczerpnięty ze strony:
 * http://www.emanueleferonato.com/2009/08/28/color-differences-algorithm/
 * 
 * Maksymalna odległość kolorów w L*ab zaczerpnięta ze strony:
 * http://stackoverflow.com/questions/19099063/what-are-the-ranges-of-coordinates-in-the-cielab-color-space
 */

package differencesearchtool;

import java.awt.Color;
import static java.lang.Math.sqrt;

public class CIE76CorrelationCalculator implements CorrelationCalculator {

    @Override
    public int calculateCorrelation(Color col1, Color col2) {
        
        double[] lab1 = COLORtoLAB(col1), lab2 = COLORtoLAB(col2);
        
        double d = Math.pow(lab1[0] - lab2[0], 2)
                + Math.pow(lab1[1] - lab2[1], 2)
                + Math.pow(lab1[2] - lab2[2], 2);

        double correlation = (1. - sqrt(d / MAX_DISTANCE_SQUARE)) * 255.;
        return (int) correlation;
    }

    // zwraca tablicę odpowiadająca kolorowi w formacie XYZ
    private static double[] RGBtoXYZ(int RGB[]) {
        
        // zamiana na liczby z przedziału 0-1
        double[] rgb = new double[]{(RGB[0] / 255.0), (RGB[1] / 255.0), (RGB[2] / 255.0)};
        
        // dostosowywanie wartości
        for (int i = 0; i < 3; i++) {
            if (rgb[i] > 0.04045)
                rgb[i] = Math.pow(((rgb[i] + 0.055) / 1.055), 2.4);
            else
                rgb[i] = rgb[i] / 12.92;
            rgb[i] *= 100;
        }
        
        double[] XYZ = new double[3];
        
        // zastosowanie macierzy
        XYZ[0] = rgb[0] * 0.4124 + rgb[1] * 0.3576 + rgb[2] * 0.1805;
        XYZ[1] = rgb[0] * 0.2126 + rgb[1] * 0.7152 + rgb[2] * 0.0722;
        XYZ[2] = rgb[0] * 0.0193 + rgb[1] * 0.1192 + rgb[2] * 0.9505;
        
        return XYZ;
    }
    
    // zwraca tablicę odpowiadająca kolorowi w formacie L*ab
    private static double[] XYZtoLAB(double XYZ[]) {
        
        // przekształcenia na podstawie parametrów białego punktu
        double[] xyz = new double[]{(XYZ[0] / 95.047), (XYZ[1] / 100.), (XYZ[2] / 108.883)};
        
        // dostosowywanie wartości
        for (int i = 0; i < 3; i++) {
            if (xyz[i] > 0.008856)
                xyz[i] = Math.pow(xyz[i], 1. / 3.);
            else
                xyz[i] = xyz[i] * 7.787 + 16. / 116.;
        }
        
        double[] LAB = new double[3];
        LAB[0] = 116 * xyz[1] - 16;
        LAB[1] = 500 * (xyz[0] - xyz[1]);
        LAB[2] = 200 * (xyz[1] - xyz[2]);
        return LAB;
    }
    
    // zwraca tablicę odpowiadająca kolorowi w formacie L*ab
    private static double[] RGBtoLAB(int RGB[]) {
        return XYZtoLAB(RGBtoXYZ(RGB));
    }
    
    // zwraca tablicę odpowiadająca kolorowi w formacie L*ab
    private static double[] COLORtoLAB(Color col) {
        int[] RGB = new int[]{col.getRed(), col.getGreen(), col.getBlue()};
        return RGBtoLAB(RGB);
    }
    
    private static double MAX_DISTANCE_SQUARE = 100. * 100. 
            + 184.439 * 184.439 
            + 202.345 * 202.345;
}
