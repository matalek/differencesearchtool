/**
 * Klasa umożliwiająca wyznaczanie różnicy obrazów na podstawie danej metody
 * wyznaczania korelacji.
 */

package differencesearchtool;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class ImageDifference {

    // przyjmuje metodę, zgodnie z którą ma wyznaczać korelację
    public ImageDifference(CorrelationCalculator method) {
        this.method = method;
    }

    // tworzy obraz w skali szarości na podstawie 2 obrazów
    public GreyscaleImage calculateDifference(BufferedImage img1, BufferedImage img2) {

        int new_width = 0, new_height = 0;
        // w przypadku równej szerokości tworzony jest obraz o rozmiarach
        // pierwszego obrazu
        if (img1.getWidth() >= img2.getWidth()) {
            new_width = img1.getWidth();
            new_height = img1.getHeight();
        } else {
            new_width = img2.getWidth();
            new_height = img2.getHeight();
        }

        GreyscaleImage result = new GreyscaleImage(new_width, new_height);
        
        // ustawienia odpowiednich pikseli
        for (int i = 0; i < new_width; ++i) {
            for (int j = 0; j < new_height; ++j) {
                if (i < img1.getWidth() && i < img2.getWidth()
                        && j < img1.getHeight() && j < img2.getHeight()) {
                    int correlation = method.calculateCorrelation(
                            new Color(img1.getRGB(i, j)), 
                            new Color(img2.getRGB(i, j)));
                    result.setGreyPixel(i, j, correlation);
                } else
                    // piksel należy tylko do jednego obrazu - piksel wynikowy
                    // będzie czarny
                    result.setGreyPixel(i, j, 0);
            }
        }
        return result;
    }

    private CorrelationCalculator method;
}
