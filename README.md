#Difference search tool#
## Project created as an contest task for ["Grasz o staż"](http://grasz.pl/) competition##

### Task (from: [official website](http://grasz.pl/zadanie/1079) - in Polish) ###
The purpose of this task is to create a Java application, which will search for differences in two images. Input images will be PNG files, their sizes can vary. Output of the application should be `result.png` file with the resolution the same as the wider image. Output image should consist of white points, where compared images are the same, and black points, where they differ. For all intermediate values you should use linear scale - the bigger correlation between images the brighter color (in grayscale).