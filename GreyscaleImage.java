/** 
 * Klasa reprezentująca obraz w skali szarości.
 */

package differencesearchtool;

import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class GreyscaleImage {

    // tworzy nowy obraz w skali szarości o podanych wymiarach
    public GreyscaleImage(int width, int height) {
        img = new BufferedImage(width, height, TYPE_INT_RGB);
    }
    
    // ustawia pojedyczy piksel (o współrzędnych (x,y)) w zależności 
    // od korelacji między kolorami
    public void setGreyPixel(int x, int y, int correlation) {
        int rgb = correlation * (256 * 256 + 256 + 1);
        img.setRGB(x, y, rgb);
    }
    
    // zapisuje obraz do pliku .png o podanej nazwie (nazwa bez rozszerzenia)
    public void write(String name) throws IOException {
        File outputfile = new File(name + ".png");
        ImageIO.write(img, "png", outputfile);
    }

    private BufferedImage img;

}
