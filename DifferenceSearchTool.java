/**
 * Rozwiązanie zadania: Wyszukiwanie różnic w dwóch obrazach.
 * Program powstał na potrzeby 20. edycji konkursu "Grasz o staż".
 * 
 * Program wyszukuje różnice w dwóch obrazach. Oczekuje podania dwóch
 * argumentów - nazw plików o rozszerzeniu .png (ale bez samego rozszerzenia). 
 * Punkty obrazu wynikowego będą koloru odpowiadającego korelacji między
 * kolorami obrazów wejściowych w tym samym punkcie (w skali szarości) - 
 * im większa korelacja, tym jaśniejszy kolor. Rozmiar obrazu wynikowego będzie
 * taki, jak rozmiar obrazu wejściowego o większej szerokości.
 * 
 * Zaimplementowane rozwiązanie umożliwia łatwe rozszerzenie programu o 
 * różne metody wyznaczania korelacji między kolorami. Interfejs
 * CorrelationCalculator odpowiada właśnie metodzie wyznaczającej korelację
 * koloró. Do tej pory zostały stworzone następujące implementacje tego
 * interfejsu:
 * - RGBCorrelationCalculator - wyznacza korelację na podstawie odległości
 * euklidesowej posczególnych czynników koloru w formacie RGB.
 * - CIE76CorrelationCalculator - wyznacza korelację na podstawie wzoru CIE76.
 * Domyślnie wybraną implementacją jest CIE76CorrelationCalculator.
 * Można wybrać inną implementację wpisując jako trzeci argument programu nazwę
 * metody (np.: RGB, CIE76).
 * 
 * Program przeznaczony jest do działania w systemie Linux.
 * 
 * Przyjęte dodatkowe założenia:
 * - jeśli obrazy wejściowe mają równą szerokość, obraz wynikowy ma rozmiary
 * takie, jak pierwszy obraz wejściowy.
 */

package differencesearchtool;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.imageio.ImageIO;

public class DifferenceSearchTool {

    // args[0] - nazwa pierwszego pliku (bez rozszerzenia)
    // args[1] - nazwa drugiego pliku (bez rozszerzenia)
    // args[2] - opcjonalny - nazwa metody obliczania korelacji
    public static void main(String[] args) {

        if (args.length < 2) {
            System.out.println("Podaj jako parametry programu nazwy dwóch "
                    + "plików o rozszerzeniu .png (bez samego rozszerzenia). "
                    + "Trzeci parametr - opcjonalny - "
                    + "nazwa metody obliczania korelacji.");
            return;
        }
        
        // wczytanie obrazów
        BufferedImage img1 = null, img2 = null;

        try {
            img1 = ImageIO.read(new File(args[0] + ".png"));
        } catch (IOException e) {
            System.err.println("Nie znaleziono pierwszego obrazu.");
            System.exit(1);
        }

        try {
            img2 = ImageIO.read(new File(args[1] + ".png"));
        } catch (IOException e) {
            System.err.println("Nie znaleziono drugiego obrazu.");
            System.exit(1);
        }
        
        // stworzenie odpowiedniej metody w zależności od podanego parametru
        CorrelationCalculator method = null;
        if (args.length >= 3) {
            try {
                Class<?> method_class = 
                        Class.forName("differencesearchtool." + args[2] + "CorrelationCalculator");
                method = (CorrelationCalculator) method_class.getConstructor().newInstance();
            } catch (ClassNotFoundException | IllegalAccessException 
                    | IllegalArgumentException | InstantiationException 
                    | NoSuchMethodException | SecurityException 
                    | InvocationTargetException ex) {
                System.err.println("Program nie wspiera tej metody wyznaczania korelacji.");
                System.exit(1);
            }
        } else
            method = new CIE76CorrelationCalculator();
        
        // wyznaczenie różnicy
        ImageDifference difference = new ImageDifference(method);
        GreyscaleImage result = difference.calculateDifference(img1, img2);
        
        // zapisanie wyniku
        try {
            result.write("result");
        } catch (IOException e) {
            System.err.println("Nie udało się zapisać pliku wynikowego.");
            System.exit(1);
        }
        
        System.out.println("Plik wynikowy stworzony pomyślnie.");
    }
}
